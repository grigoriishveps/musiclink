import React, { useEffect } from 'react'
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom'

import MainLayout from '@app/layout/MainLayout/MainLayout'
import MainPage from '@app/main/MainPage'
import CssBaseline from '@mui/material/CssBaseline'
import LoginPage from '@app/auth/pages/LoginPage'
import RegistrationPage from '@app/auth/pages/RegistrationPage'
import NotFoundPage from '@app/NotFoundPage'
import PostsPage from '@app/posts/pages/PostsPage'
import PostInfoPage from '@app/posts/pages/PostInfoPage'
import PostEditPage from '@app/posts/pages/PostEditPage'
import { useStore } from 'effector-react'
import { $authStore } from '@app/stores/authStore'
import { formatAuthToken, setDefaultRequestHeader } from '@app/common/api'
import PostCreatePage from '@app/posts/pages/PostCreatePage'
import MyPostsPage from '@app/posts/pages/MyPostsPage'

const Root = () => {
  const authStore = useStore($authStore)

  useEffect(() => {
    if (authStore.isAuthenticated) {
      setDefaultRequestHeader('Authorization', formatAuthToken(authStore.token))
    } else {
      setDefaultRequestHeader('Authorization', null)
    }
  }, [authStore.isAuthenticated])

  return (
    <BrowserRouter>
      <CssBaseline />
      <Switch>
        <Route path="/login">
          <LoginPage/>
        </Route>
        <Route path="/register">
          <RegistrationPage/>
        </Route>
        <Route path="*">
          <MainLayout>
            <Switch>
              <Route exact path="/">
                <MainPage />
              </Route>
              <Route exact path="/posts">
                <PostsPage />
              </Route>
              <Route exact path="/posts/my">
                <MyPostsPage />
              </Route>
              <Route exact path="/posts/new">
                <PostCreatePage />
              </Route>
              <Route exact path="/posts/:id">
                <PostInfoPage />
              </Route>
              <Route exact path="/posts/:id/edit">
                <PostEditPage />
              </Route>
              <Route path="*">
                <NotFoundPage/>
              </Route>
            </Switch>
          </MainLayout>
        </Route>
      </Switch>
    </BrowserRouter>
  )
}

export default Root
