import React from 'react';
import cx from 'classnames'
import {AppBar, Box, Toolbar, Typography} from "@mui/material";

import IconButton from '@mui/material/IconButton'
import MenuIcon from '@mui/icons-material/Menu';

import styles from './header.module.scss'
import Button, { ButtonColor } from '@app/ui/Button/Button'

import { $authStateUpdate} from '@app/stores/authStore'
import { AuthStates } from '@app/auth/AuthStore'

const Header = ({
  openNavigation,
  onNavigationToggle,
}) => {
  const headerClassName = cx(styles.header, {
    [styles.open]: openNavigation,
  })

  const handleLogout = async () => {
    // await authStore.logout()
    $authStateUpdate(AuthStates.UNAUTHENTICATED)
    // await applicationStore.setAdminMode(AdminMode.OFF)
    // Storage.delete('adminMode')
    //
    // history.push(LoginRoute.path)
  }

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="fixed" className={headerClassName}>
        <Toolbar
          variant="dense"
          className={styles.content}
        >
          <If condition={!openNavigation}>
            <IconButton
              aria-label="open drawer"
              edge="start"
              className={styles.navigation}
              onClick={onNavigationToggle}
            >
              <MenuIcon />
            </IconButton>
          </If>
          <Typography className={styles.navigationText}>
            {/*{title}*/}
            Title
          </Typography>

          <Button
            color={ButtonColor.SECONDARY}
            onClick={handleLogout}
          >
            Logout
          </Button>
        </Toolbar>
      </AppBar>
    </Box>
  )
}

export default Header
