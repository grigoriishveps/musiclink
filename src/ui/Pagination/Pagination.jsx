import React from 'react'

import MuiPagination from '@mui/material/Pagination'

const Pagination = ({
  page,
  count,
  onChange,
}) => {

  const handleChangePage = (event, newPage) => {
    onChange(newPage)
  }

  return (
    <MuiPagination
      count={count}
      page={page}
      onChange={handleChangePage}
    />
  )
}

export default Pagination
