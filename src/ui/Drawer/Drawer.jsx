import React from 'react'
import { useHistory } from 'react-router-dom'
import cx from 'classnames'

import MuiDrawer from '@mui/material/Drawer'
import Divider from '@mui/material/Divider'
import IconButton from '@mui/material/IconButton'

import ChevronLeftIcon from '@mui/icons-material/ChevronLeft'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemText from '@mui/material/ListItemText'

import styles from './drawer.module.scss'


const listNavigation = [
  {
    label: 'Main',
    path: '/',
    // ready: true,
    //color: 'aquamarine',
  },
  {
    label: 'Posts',
    path: '/posts',
    // ready: true,
    //color: 'aquamarine',
  },
  {
    label: 'My posts',
    path: '/posts/my',
  },
  {
    label: 'New post',
    path: '/posts/new',
  },
]

const Drawer = ({ open, onClose }) => {
  let history = useHistory();

  const handleNavigate = (path) => {
    return () => {
      history.push(path)
    }
  }

  return (
    <MuiDrawer
      className={styles.drawer}
      variant="persistent"
      anchor="left"
      open={open}
    >
      <div className={styles.header}>
        <IconButton onClick={onClose}>
          <ChevronLeftIcon />
        </IconButton>
      </div>
      <Divider />
      <List>
        <For of={listNavigation} body={(route) => (
          <ListItem
            button
            className={cx(route.ready? styles.done : styles.failure, route.color)}
            styles={route.ready? {background: route.color}:{}}
            key={route.label}
            onClick={handleNavigate(route.path)}
          >
            <ListItemText primary={route.label} />
          </ListItem>
        )}
        />
      </List>
    </MuiDrawer>
  )
}

export default Drawer
