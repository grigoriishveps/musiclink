import React from 'react'

import LoadingButton from '@mui/lab/LoadingButton'

export const ButtonColor = {
  PRIMARY: 'primary',
  ERROR: 'error',
  SECONDARY : 'secondary',
  WARNING : 'warning'
}

export const ButtonVariant = {
  CONTAINED: 'contained',
  OUTLINED: 'outlined',
  TEXT: 'text',
}

const Button = ({
  type = 'button',
  children,
  variant = ButtonVariant.CONTAINED,
  color = ButtonColor.PRIMARY,
  loading = false,
  className,
  isUpload = false,
  ...props
}) => {

  return (
    <LoadingButton
      className={className}
      type={type}
      variant={variant}
      color={color}
      loading={loading}
      component={isUpload ? 'span' : 'button'}
      {...props}
    >
      {children}
    </LoadingButton>
  )
}

export default Button
