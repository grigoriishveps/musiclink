// import { action, computed, observable } from 'mobx'
//
// import * as R from 'ramda'
//
// import Storage from 'common/storage'
//
// import API from 'common/api'
// import logger from 'lib/logger'
//
// import {
//   is401Reseponse,
//   setDefaultRequestHeader,
//   formatAuthorizationHeader,
//   installResponseInterceptor,
// } from 'common/utils'
//
// import { MAX_RETRIES } from 'common/constants'
// import { LoginResponse, LoginRequest } from 'common/api/auth/login'

const StorageKey = {
  REFRESH_TOKEN: 'refreshToken',
}

export const AuthStates = {
  UNAUTHENTICATED: 'unauthenticated',
  IN_PROGRESS: 'inProgress',
  AUTHENTICATED: 'authenticated',
}

// class AuthStore {
//   @observable
//   authState = null
//
//   retries = 0
//
//   constructor() {
//     installResponseInterceptor(R.identity, this.handle401)
//   }
//
//   get refreshToken() {
//     return Storage.get(StorageKey.REFRESH_TOKEN)
//   }
//
//
//
//   @action
//   async authorize({ force = false } = {}) {
//     if (!force && this.authState === AuthStates.AUTHENTICATED) {
//       return
//     }
//
//     const refreshToken = this.refreshToken
//
//     if (!refreshToken) {
//       this.cleanup()
//
//       return
//     }
//
//     this.authState = AuthStates.IN_PROGRESS
//
//     try {
//       const result = await API.token.refresh({ refreshToken })
//
//       this.handleTokenResult(result)
//
//       this.authState = AuthStates.AUTHENTICATED
//     } catch (e) {
//       logger.error(e)
//
//       this.cleanup()
//     }
//   }
//
//   @action
//   async login(data) {
//     const result = await API.user.login(data)
//
//     this.handleTokenResult(result)
//
//     this.authState = AuthStates.AUTHENTICATED
//   }
//
//   @action
//   async logout() {
//     if (!this.isAuthenticated) {
//       return
//     }
//     // await API.user.logout({})
//
//     this.cleanup()
//   }
//
//   @action
//   cleanup() {
//     Storage.delete(StorageKey.REFRESH_TOKEN)
//
//     setDefaultRequestHeader('Authorization', null)
//
//     this.authState = AuthStates.UNAUTHENTICATED
//   }
//
//   @action
//   handleTokenResult({ credentials: { accessToken, refreshToken } }) {
//     setDefaultRequestHeader('Authorization', formatAuthorizationHeader(accessToken))
//
//     Storage.set(StorageKey.REFRESH_TOKEN, refreshToken)
//   }
//
//   handle401 = async (error) => {
//     if (!is401Reseponse(error?.response)) {
//       throw error
//     }
//
//     this.retries += 1
//
//     if (this.retries > MAX_RETRIES) {
//       throw Error('max auth retries exceeded')
//     }
//
//     await this.authorize()
//   }
// }
//
// export default AuthStore
