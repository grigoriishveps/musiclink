import React, { useEffect } from 'react'
import { observer } from 'mobx-react'
import { Redirect } from 'react-router'

import LoadingComponent from '@app/ui/Loading/LoadingPage'
import { useStore } from 'effector-react'
import { $authStore} from '@app/stores/authStore'

const AuthHOC = (Component) =>{
  const Auth = ({ children }) => {
    const authStore = useStore($authStore)

    // useEffect(() => {
    //   authStore.authorize()
    // }, [authStore])

    return (
      // <Component >
      //   {children}
      // </Component>
      <Choose>
        <When condition={authStore.isAuthenticated}>
          <Component >
            {children}
          </Component>
        </When>
        <When condition={authStore.isAuthenticating}>
          <LoadingComponent />
        </When>
        <Otherwise>
          <Redirect to="/login" />
        </Otherwise>
      </Choose>
    )
  }

  return observer(Auth)
}

export default AuthHOC
