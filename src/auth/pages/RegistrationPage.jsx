import React from 'react'
import { Redirect, useHistory } from 'react-router-dom'
import { useFormik } from 'formik'
import { useSnackbar } from 'notistack'

import locale from '@app/locale/strings'
import styles from './loginPage.module.scss'
import {Button, Container} from "@mui/material";
import TextField from "@app/ui/TextField/TextField";
import { $authStore } from '@app/stores/authStore'
import { useStore } from 'effector-react'

import { useMutationRegister } from '@app/common/api'

const RegistrationPage = () => {
  const authStore = useStore($authStore)
  const { enqueueSnackbar } = useSnackbar()
  let history = useHistory();
  const registerMutation = useMutationRegister()

  const handleRegistration = async ({ email, password, name }) => {
    await registerMutation.mutate({
      email,
      password,
      name,
    }, {
      onSuccess: async ({ data }) => {
        enqueueSnackbar('Success authorization', {
          variant: 'success',
        });

        history.push('/login')
      },
      onError: () => {
        enqueueSnackbar('Something went wrong', {
          variant: 'error',
        });
      }
    }).

    history.push('/')

    Storage.delete('adminMode')
  }

  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
      name: '',
    },
    onSubmit: handleRegistration,
  })

  if (authStore.isAuthenticated) {
    return <Redirect to="/" />
  }

  const handleLogin = () => {
    history.push('/login')
  }

  return (
    <Container
      className={styles.container}
      maxWidth="xs"
    >
      <form onSubmit={formik.handleSubmit} className={styles.form}>
        <h2 className={styles.headerText}>
          {locale.common.signUp}
        </h2>
        <TextField
          required
          fullWidth
          autoFocus
          id="name"
          label="Name"
          name="name"
          autoComplete="name"
          value={formik.values.name}
          onChange={formik.handleChange}
        />
        <TextField
          required
          fullWidth
          autoFocus
          id="email"
          label="Email Address"
          name="email"
          autoComplete="email"
          value={formik.values.email}
          onChange={formik.handleChange}
        />
        <TextField
          required
          fullWidth
          name="password"
          label="Password"
          type="password"
          id="password"
          autoComplete="current-password"
          value={formik.values.password}
          onChange={formik.handleChange}
        />
        <div className={styles.row}>
          <Button
            type="button"
            variant={'outlined'}
            className={styles.button}
            onClick={handleLogin}
          >
            Login
          </Button>
          <Button
            type="submit"
            variant={'contained'}
            className={styles.button}
          >
            Sign Up
          </Button>
        </div>
      </form>
    </Container>
  )
}

export default RegistrationPage
