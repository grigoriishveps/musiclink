import React, { useEffect } from 'react'
import { Redirect, useHistory, useLocation } from 'react-router-dom'
import { useFormik } from 'formik'
import { useSnackbar } from 'notistack'

import locale from '@app/locale/strings'
import styles from './loginPage.module.scss'
import {Button, Container} from "@mui/material";
import TextField from "@app/ui/TextField/TextField";
import { $authStateUpdate, $authStore, $tokenStateUpdate } from '@app/stores/authStore'
import { useStore } from 'effector-react'
import { AuthStates } from '@app/auth/AuthStore'
import { useMutationLogin } from '@app/common/api'
import LoginDebug from '@app/auth/pages/LoginDebug'

const LoginPage = () => {
  const authStore = useStore($authStore)
  const { enqueueSnackbar } = useSnackbar()
  let history = useHistory();
  let location = useLocation();
  const loginMutation = useMutationLogin()

  const handleLogin = async ({ email, password }) => {
    await loginMutation.mutate({
      username: email,
      password,
    }, {
      onSuccess: async ({ data }) => {
        console.log(data.access_token)
        $tokenStateUpdate(data.access_token)
        $authStateUpdate(AuthStates.AUTHENTICATED)

        enqueueSnackbar('Success authorization', {
          variant: 'success',
        });
      },
      onError: () => {
        enqueueSnackbar('Something went wrong', {
          variant: 'error',
        });
      }
    }).

    history.push('/')

    Storage.delete('adminMode')
  }

  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    onSubmit: handleLogin,
  })

  if (authStore.isAuthenticated) {
    return <Redirect to="/" />
  }

  const handleRegister = () => {
    history.push('/register')
  }

  return (
    <Container
      className={styles.container}
      maxWidth="xs"
    >
      <form onSubmit={formik.handleSubmit} className={styles.form}>
        <h2 className={styles.headerText}>
          {locale.common.signIn}
        </h2>
        <TextField
          required
          fullWidth
          autoFocus
          id="email"
          label="Email Address"
          name="email"
          autoComplete="email"
          value={formik.values.email}
          onChange={formik.handleChange}
        />
        <TextField
          required
          fullWidth
          name="password"
          label="Password"
          type="password"
          id="password"
          autoComplete="current-password"
          value={formik.values.password}
          onChange={formik.handleChange}
        />
        <div className={styles.row}>
          <Button
            type="button"
            variant={'outlined'}
            className={styles.button}
            onClick={handleRegister}
          >
            Register
          </Button>
          <Button
            type="submit"
            variant={'contained'}
            className={styles.button}
          >
            Sign In
          </Button>
        </div>
      </form>
      <If condition={/debug=1/.test(location.search)}>
        <LoginDebug />
      </If>
    </Container>
  )
}

export default LoginPage
