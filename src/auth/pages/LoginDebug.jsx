import React, { useEffect } from 'react'
import { Redirect, useHistory, useLocation } from 'react-router-dom'
import { useFormik } from 'formik'
import { useSnackbar } from 'notistack'

import locale from '@app/locale/strings'
import styles from './loginPage.module.scss'
import {Button, Container} from "@mui/material";
import TextField from "@app/ui/TextField/TextField";
import { useDebugMutation } from '@app/common/api'

const LoginDebug = () => {
  const { enqueueSnackbar } = useSnackbar()

  const loginMutation = useDebugMutation()

  const handleLogin = async ({ email }) => {
    await loginMutation.mutate({
      domen: email,
    }, {
      onSuccess: async ({ data }) => {
        enqueueSnackbar('Success authorization', {
          variant: 'success',
        });
      },
      onError: () => {
        enqueueSnackbar('Something went wrong', {
          variant: 'error',
        });
      }
    }).

    history.push('/')

    Storage.delete('adminMode')
  }

  const formik = useFormik({
    initialValues: {
      email: 'dasdasd',
    },
    onSubmit: handleLogin,
  })

  return (
    <Container
      className={styles.container}
      maxWidth="xs"
    >
      <form onSubmit={formik.handleSubmit} className={styles.form}>
        <h2 className={styles.headerText}>
          Debug API
        </h2>
        <TextField
          required
          fullWidth
          autoFocus
          id="email"
          label="Email Address"
          name="email"
          autoComplete="email"
          value={formik.values.email}
          onChange={formik.handleChange}
        />
        <div className={styles.row}>
          <Button
            type="submit"
            variant={'contained'}
            className={styles.button}
          >
            Sign In
          </Button>
        </div>
      </form>
    </Container>
  )
}

export default LoginDebug
