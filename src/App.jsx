import React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import { QueryClient, QueryClientProvider } from 'react-query'
import { SnackbarProvider } from 'notistack'
import Root from '@app/Root'

const queryClient = new QueryClient({
  defaultOptions: {
    queries:{
      refetchOnWindowFocus: false,
    },
  },
})

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <CssBaseline />
      <SnackbarProvider maxSnack={3}>
        <Root />
      </SnackbarProvider>
    </QueryClientProvider>
  );
}

export default App;
