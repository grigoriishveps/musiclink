import React, { useState } from 'react'
import cx from 'classnames'
import * as R from 'ramda'

import Header from '../../ui/Header/Header'
import AuthHOC from '@app/auth/AuthHOC'
import Drawer from '@app/ui/Drawer/Drawer'

import styles from './mainLayout.module.scss'

const MainLayout = ({children}) => {
  const [openNavigation, setOpenNavigation] = useState(false)

  const handleDrawerToggle = () => {
    setOpenNavigation(R.not)
  }

  const contentClassName = cx(styles.content, {
    [styles.open]: openNavigation,
  })

  return (
    <div className={styles.container}>
      <Header
        openNavigation={openNavigation}
        onNavigationToggle={handleDrawerToggle}
      />
      <Drawer
        open={openNavigation}
        onClose={handleDrawerToggle}
      />
      <div
        id="mainLayoutContainer"
        className={contentClassName}
      >
        {children}
      </div>
    </div>
  );
};

export default MainLayout |> AuthHOC
