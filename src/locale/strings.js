export default {
  page: {
    companies: {
      info: {
        applications: 'Applications',
      },
    },
    error: {
      header: 'Ошибка',
    },
  },
  header: {
    company: 'Company',
    app: 'App',
    user: 'User',
  },
  navigation: {
    user: 'User',
    users: 'Users',
  },
  common: {
    loading: 'loading',
    signIn: 'Sign In',
    signUp: 'Sign Up',
    logout: 'Logout',
    cancel: 'Cancel',
    download: 'Download',
    save: 'Save',
    delete: 'Delete',
    adminOn: 'Admin ON',
    adminOff: 'Admin OFF',
    search: 'Search',
    noFilters: 'No filters applied',
    errorLoading: 'Error Loading',
  },
  fields: {
    name: 'Name',
    email: 'Email',
    password: 'Password',
    linkPayout: 'Link Payout',
    publickKey: 'Public Key',
    privateKey: 'Private Key',
    styngPrice: 'Styng Price',
    stynglistPrice: 'Stynglist Price',
    currency: 'Currency',
    company: 'Company',
    styngCurrency: 'Styng Currency',
    stynglistCurrency: 'Stynglist Currency',
    apiKey: 'API Key',

  },
}
