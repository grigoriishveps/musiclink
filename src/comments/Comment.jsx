import React, { memo } from 'react'
import Fade from 'react-reveal/Fade'
import * as R from 'ramda'
import { useHistory } from 'react-router-dom'
import { DateTime } from 'luxon'

import AccountCircleIcon from '@mui/icons-material/AccountCircle';

import styles from './comment.module.scss'
import { useUserQuery } from '@app/common/api'

const Comment = ({ comment, userId, date }) => {
  let history = useHistory()
  const { data: user = {name: 'User'}} = useUserQuery(userId)

  const handleReload = () => {
    document.location.reload()
  }

  const handleUserClick = () => {
    history.push(`/posts?userId=${userId}`)
  }

  return (
      <div
        className={styles.mainWrapper}
      >
        <Fade bottom>
          <div className={styles.header}>
            <AccountCircleIcon />
            <div>
              <h5 className={styles.author} onClick={handleUserClick}>
                {user.name}
              </h5>
              <h6 className={styles.date}>
                  {DateTime.fromISO(date).toFormat('LLL dd, yyyy   hh:mm')}
              </h6>
            </div>
          </div>
          <div className={styles.content}>
            {comment}
          </div>
        </Fade>
      </div>
  )
}

export default memo(Comment)
