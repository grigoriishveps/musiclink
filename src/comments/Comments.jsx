import React, { useState } from 'react'
import Fade from 'react-reveal/Fade'
import * as R from 'ramda'
import { useHistory } from 'react-router-dom'

import styles from './comments.module.scss'
import { useCommentMutation, useCommentsQuery } from '@app/common/api'
import Button from '@app/ui/Button/Button'
import { useQueryClient } from 'react-query'
import { TextField } from '@mui/material'
import { data } from 'autoprefixer'
import Comment from '@app/comments/Comment'
import GroupIcon from '@mui/icons-material/Group';

const Comments = ({ postId }) => {
  let history = useHistory()
  const client = useQueryClient()
  const [newComment, setNewComment] = useState('')
  const { data: comments = [] } = useCommentsQuery(postId)
  const mutation = useCommentMutation(postId)

  console.log(comments)
  const handleReload = () => {
    document.location.reload()
  }

  const handleChangeComment = (event) => {
    setNewComment(event.target.value);
  }

  const handleSendComment = async () => {
    if (!R.isEmpty(newComment)) {
      mutation.mutate({
        text: newComment,
      }, {
        onSuccess: () => {
          client.invalidateQueries('comments')
        }
      })
    }
  }

  return (
    <div
      className={styles.mainWrapper}
    >
      <div className={styles.titleContainer}>
          Comments
        <span>
          {comments.length}
        </span>
      </div>
      <div className={styles.content}>
        <For each="comment" of={comments}>
          <Comment
            key={comment.id}
            userId={comment.userId}
            comment={comment.text}
            date={comment.date}
          />
        </For>
      </div>
      <div className={styles.editContainer}>
        <div className={styles.title}>
          Write comment
        </div>
        <div className={styles.editor}>
          <TextField
            multiline
            fullWidth
            minRows={4}
            value={newComment}
            onChange={handleChangeComment}
            variant="outlined"
          />
        </div>
        <Button
          variant={"outlined"}
          onClick={handleSendComment}
        >
          Send
        </Button>
      </div>
    </div>
  )
}

export default Comments
