import EventEmitter from 'events'

class Storage extends EventEmitter {
  static eventType = 'storage'

  constructor() {
    super()

    window.addEventListener(Storage.eventType, this.onSessionIdChange)
  }

  get(key) {
    return window.localStorage.getItem(key)
  }

  set(key, value) {
    window.localStorage.setItem(key, value)
  }

  delete(key) {
    window.localStorage.removeItem(key)
  }

  onSessionIdChange = ({ type, key, newValue }) => {
    if (type === Storage.eventType) {
      this.emit('change', key, newValue)
    }
  }

  close() {
    window.removeEventListener(Storage.eventType, this.onSessionIdChange )
  }
}

export default new Storage()
