import { useQuery, useMutation } from 'react-query'
import * as R from 'ramda'
import qs from 'qs'
import axios from 'axios'
import { useStore } from 'effector-react'
import { $authStore } from '@app/stores/authStore'

const domen = 'https://api.master.mymusiclink.xyz'
// const domen = '/api'

const toPost = (data) => ({
  id: data.id,
  title: data.title,
  description: data.text,
  date: data.date,
  userId: data.user,
})

const toComment = ({text, date, user, id}) => ({
  id,
  text,
  date,
  userId: user,
})

export const setDefaultRequestHeader = (key, value) => {
  axios.defaults.headers.common[key] = value
}

export const formatAuthToken = accessToken => `Bearer ${accessToken}`

export const usePostsQuery = (userId) => {
  return useQuery(['posts', userId], async () => {
    const { data } = await axios.get(`${domen}/posts/?user_id=${userId}`)

    return R.map(toPost, data.items)
  }, {
    retry: 1,
    keepPreviousData: true,
  })
}

export const usePostQuery = (id) => {
  return useQuery(['post', id], async () => {
    const { data } = await axios.get(`${domen}/posts/${id}`)

    return toPost(data)
  }, {
    retry: 1,
    keepPreviousData: true,
    refetchOnWindowFocus: false,
  })
}

export const usePostMutation = (isNew = true) => {
  return useMutation((body) =>{
    if (isNew) {
      return axios.post(`${domen}/posts/`, body)
    } else {
      return axios.patch(`${domen}/posts/${body.id}`, body)
    }
  })
}

export const useCommentsQuery = (id) => {
  return useQuery(['comments', id], async () => {
    const { data } = await axios.get(`${domen}/comments/${id}`)

    return R.map(toComment, data)
  }, {
    retry: 1,
    keepPreviousData: true,
  })
}

export const useMeQuery = () => {
  const { token } = useStore($authStore)
  return useQuery(['user-me', token], async () => {
    const { data } = await axios.get(`${domen}/users/me`)

    return data
  }, {
    retry: 1,
    keepPreviousData: true,
  })
}

export const useUserQuery = (id) => {
  return useQuery(['user', id], async () => {
    const { data } = await axios.get(`${domen}/users/new/${id}`)

    return data
  }, {
    retry: 1,
    keepPreviousData: true,
  })
}

export const useMutationLogin = () => {
  return useMutation((body) => axios.post(`${domen}/auth/login`, qs.stringify(body), {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    }
  }))
}

export const useMutationRegister = () => {
  return useMutation((body) => axios.post(`${domen}/auth/register`, body))
}

export const useCommentMutation = (id) => {
  return useMutation((body) => axios.post(`${domen}/comments/${id}`, body))
}

export const useDebugMutation = () => {
  return useMutation((body) => axios.post(`${body.domen}/check`, qs.stringify(body)))
}

