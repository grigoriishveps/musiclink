import {createEvent, createStore, combine} from 'effector'
import * as R from 'ramda'

export const $page = createStore(1)
export const $paginationUpdate = createEvent()
$page.on($paginationUpdate, (state, data) => data)

const $pageSize = createStore(30)

const $filter = createStore('')
export const $filterUpdate = createEvent()
$filter.on($filterUpdate, (state, data) => data)

export const $authStore = combine({
  page: $page,
  pageSize: $pageSize,
  filter: $filter,
})
