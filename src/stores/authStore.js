import {createEvent, createStore, combine} from 'effector'
import { AuthStates } from '@app/auth/AuthStore'
import * as R from 'ramda'
import { persist } from 'effector-storage/local'

export const $authState = createStore(AuthStates.UNAUTHENTICATED, { name: 'auth'})
export const $token = createStore('', { name: 'token'})

const isAuthenticated = $authState.map(x => x === AuthStates.AUTHENTICATED)
const isAuthenticating = $authState.map(x => x === AuthStates.IN_PROGRESS || R.isNil($authState))

export const $authStateUpdate = createEvent()
export const $tokenStateUpdate = createEvent()

$authState.on($authStateUpdate, (state, data) => data)
$token.on($tokenStateUpdate, (state, data) => data)

export const $authStore = combine({
  authState: $authState,
  token: $token,
  isAuthenticated,
  isAuthenticating
})

persist({ store: $authState })
persist({ store: $token })
