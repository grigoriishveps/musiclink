import React from 'react'
import Fade from 'react-reveal/Fade'
import { useHistory } from 'react-router-dom'

import ReadOnlySlate from '@app/ui/SlateRich/ReadOnlySlate'
import ReturnImage from './returnArrow.svg'

import styles from './postInfo.module.scss'
import { useMeQuery } from '@app/common/api'
import { Button } from '@mui/material'

const PostInfo = ({ news }) => {
  let history = useHistory()
  const {data : me} = useMeQuery()

  const handleReturnNewsroom = async () => {
    history.push('/posts')
  }
  console.log(me, news)
  const handleEdit = async () => {
    history.push(`${history.location.pathname}/edit`)
  }

  return (
    <React.Fragment>
      <div className={styles.header}>
        <div className={styles.returnContainer} onClick={handleReturnNewsroom}>
          <ReturnImage />
          <span className={styles.returnText}>
            Posts
          </span>
        </div>
        <If condition={news.userId === me?.id}>
          <Button
            className={styles.edit}
            variant={'contained'}
            onClick={handleEdit}
          >
            Edit
          </Button>
        </If>
      </div>
      <div className={styles.content}>
        <Fade bottom>
          <h5 className={styles.author}>
            Author
          </h5>
          <h1 className={styles.header}>
            {news.title}
          </h1>
        </Fade>
        <Fade bottom>
          <div className={styles.infoContainer}>
            <ReadOnlySlate value={JSON.parse(news.description)}/>
          </div>
        </Fade>
      </div>
    </React.Fragment>
  )
}

export default PostInfo
