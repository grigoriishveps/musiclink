import React, { useRef } from 'react'
import { useHistory } from 'react-router-dom'
import { DateTime } from 'luxon'

import styles from './newsCard.module.scss'
import ReadOnlySlate from '@app/ui/SlateRich/ReadOnlySlate'

const NewsCard = ({ card }) => {
  let history = useHistory()
  const ref = useRef(null)

  const handleClickCard = () => {
    history.push(`/posts/${card.id}`)
  }

  return (
    <div
      className={styles.container}
      ref={ref}
      onClick={handleClickCard}
    >
      <h2 className={styles.title}>
        {card.title}
      </h2>
      <div className={styles.preview}>
        <ReadOnlySlate value={JSON.parse(card.description)}/>
      </div>
      <div className={styles.link}>
        Learn more
      </div>
    </div>
  )
}

export default NewsCard
