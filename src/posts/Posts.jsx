import React, { useState } from 'react'
import * as R from 'ramda'
import axios from 'axios'

import styles from './posts.module.scss'
import NewsCard from '@app/posts/newsCard/NewsCard'
import Otherwise from 'ramda/src/otherwise'
import { useMeQuery } from '@app/common/api'

const Posts =  ({ news, user }) => {
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(false)
  const [allNews, setAllNews] = useState(null)
  const {data : me} = useMeQuery()

  return (
    <React.Fragment>
      <div
        className={styles.mainWrapper}
      >
        <h2>
          <Choose>
            <When condition={me?.id === user?.id}>
              My Posts
            </When>
            <When condition={!R.isNil(user)}>
              { `Posts of ${user?.name}`}
            </When>
            <Otherwise>
              Posts
            </Otherwise>
          </Choose>
        </h2>
        <Choose>
          <When condition={news.length !== 0}>
            <div className={styles.news}>
              <For each="oneNews" of={allNews ?? news}>
                <NewsCard card={oneNews} />
              </For>
            </div>
          </When>
        </Choose>
      </div>
    </React.Fragment>
  )
}

export default Posts
