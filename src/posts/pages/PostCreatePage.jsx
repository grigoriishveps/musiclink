import React from 'react'

import EditPost from '@app/posts/editPost/EditPost'

const PostEditPage = () => {
  return (
    <EditPost isNew />
  )
}

export default PostEditPage
