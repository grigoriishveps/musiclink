import React from 'react'
import { useMeQuery } from '@app/common/api'
import LoadingRemote from '@app/common/LoadingRemote'

import { Redirect } from 'react-router-dom'

const PostsPage = () => {
  const {data, status} = useMeQuery()

  return (
    <LoadingRemote status={status}>
      <Redirect to={`/posts/?userId=${data?.id}`} />
    </LoadingRemote>
  )
}

export default PostsPage
