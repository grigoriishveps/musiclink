import React from 'react'
import { useParams } from 'react-router-dom'

import LoadingRemote from '@app/common/LoadingRemote'
import { usePostQuery } from '@app/common/api'
import PostInfo from '@app/posts/postInfo/PostInfo'
import Comments from '@app/comments/Comments'
import styles from '@app/posts/postInfo/postInfo.module.scss'

const PostInfoPage = () => {
  let { id } = useParams();
  const {data, status} = usePostQuery(Number(id))

  return (
    <LoadingRemote status={status}>
      <div
        className={styles.mainWrapper}
      >
        <PostInfo news={data ?? {}} />
        <Comments postId={id} />
      </div>
    </LoadingRemote>
  )
}

export default PostInfoPage
