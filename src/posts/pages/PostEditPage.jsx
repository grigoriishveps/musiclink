import React from 'react'
import { useParams } from 'react-router-dom'

import { usePostQuery } from '@app/common/api'
import LoadingRemote from '@app/common/LoadingRemote'
import EditPost from '@app/posts/editPost/EditPost'

const PostEditPage = () => {
  let { id } = useParams();
  const {data, status} = usePostQuery(Number(id))

  return (
    <LoadingRemote status={status}>
      <EditPost post={data ?? {}} />
    </LoadingRemote>
  )
}

export default PostEditPage
