import React from 'react'
import { usePostsQuery, useUserQuery } from '@app/common/api'
import LoadingRemote from '@app/common/LoadingRemote'
import Posts from '@app/posts/Posts'
import { useQuery } from '@app/common/constants'

const PostsPage = () => {
  const query = useQuery()
  const {data, status} = usePostsQuery(query.get("userId"))
  const {data: user} = useUserQuery(query.get("userId"))

  return (
    <LoadingRemote status={status}>
      <Posts news={data ?? []} user={user}/>
    </LoadingRemote>
  )
}

export default PostsPage
