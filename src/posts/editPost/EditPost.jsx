import React, { useState } from 'react'
import Fade from 'react-reveal/Fade'
import * as R from 'ramda'
import { useHistory } from 'react-router-dom'
import { useMutation, useQueryClient } from 'react-query'
import { createEditor } from 'slate'
import { Slate, Editable, withReact } from 'slate-react'
import { Button, TextField } from '@mui/material'

import RichTextExample from '@app/ui/SlateRich/Slate'
import { usePostMutation } from '@app/common/api'

import styles from './editPost.module.scss'
import Otherwise from 'ramda/src/otherwise'

const EditPost = ({ post = {
  title: '',
  description: JSON.stringify([{
    type: 'paragraph',
    children: [
      {
        text: '',
      },
    ],
  },])
},
  isNew = false
}) => {
  const [title, setTitle] = useState(post.title)
  const [text, setText] = useState(post.description)
  const client = useQueryClient()
  const mutation = usePostMutation(isNew)
  let history = useHistory()

  const handleChangeTitle = (event) => {
    setTitle(event.target.value);
  }

  const handleSubmit = () => {
    if (!R.isEmpty(title) && !R.isEmpty(text)) {
      mutation.mutate({
        title,
        text,
        id: post?.id
      }, {
        onSuccess: () => {
          client.invalidateQueries('posts')
          history.push('/posts/my')
        }
      })
    }
  }

  return (
    <React.Fragment>
      <div
        className={styles.mainWrapper}
      >
        <Choose>
          <When condition={isNew}>
            <h2>
              New Post
            </h2>
          </When>
          <Otherwise>
            <h2>
              Edit Post
            </h2>
          </Otherwise>
        </Choose>
        <div className={styles.title}>
          <TextField
            fullWidth
            label={"Title"}
            value={title}
            onChange={handleChangeTitle}
            variant="outlined"
          />
        </div>
        <h4>
          description
        </h4>
        <div
          className={styles.description}
        >
          <RichTextExample initialValue={JSON.parse(post.description)} setText={setText}/>
        </div>
        <Button
          className={styles.button}
          variant={'contained'}
          onClick={handleSubmit}
        >
          Save
        </Button>
      </div>
    </React.Fragment>
  )
}

export default EditPost
