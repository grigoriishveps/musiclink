const express = require('express');
const path = require('path');
const port = process.env.PORT || 8080;
const app = express();

const DIST_DIR = path.join(__dirname, "dist");

// the __dirname is the current directory from where the script is running
app.use(express.static(DIST_DIR));

// send the user to index html page inspite of the url
app.get('*', (req, res) => {
  res.sendFile(path.resolve(DIST_DIR, 'index.html'));
});

app.listen(port);
